const router = require('express').Router();

/* GET users listing. */
// router.get('/', function (req, res, next) {
//   res.send('respond with a resource');
// });

const {
  loginJwt,
  register,
  restrictTo,
  whoami
} = require('../controllers/authController');
const {
  getAllUsers,
  getUser,
  getUserHistory,
  updateUser,
  deleteUser,
} = require('../controllers/userController')
// const passport = require("../lib/passport")
const restrict = require('../middlewares/restrict')

/* Admin Page */
// router.get('/admin-dashboard', restrict, adminPage);

// Register a user
// router.get('/register', (req, res) => res.render('register'));
router.post('/register', register);
router.post('/api/v1/auth/register', register)

// Login page using JWT
// router.get('/login', (req, res) => res.render('login'))
router.post('/login', loginJwt)
router.post('/api/v1/auth/login', loginJwt)
// and seek for profile page
router.get('/profile', restrict, whoami); // can be improved by using .render('profile')
router.get('/api/v1/auth/whoami', restrict, whoami)

// GET Users
router.get('/:id', restrict, getUser);
router.get('/:id/histories', restrict, getUserHistory);

// SuperAdmin
router.use(restrictTo('admin'));
router.get('/', restrict, getAllUsers);
router.route('/:id', restrict).get(getUser).patch(updateUser).delete(deleteUser);


module.exports = router;
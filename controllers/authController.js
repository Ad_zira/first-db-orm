// const {
//   User
// } = require('../models')
// const passport = require('../lib/passport');
// const AppError = require('../utils/appError');

// function formatUser(user) {
//   const {
//     id,
//     username
//   } = user;
//   return {
//     id,
//     username,
//     accessToken: user.generateToken()
//   }
// };

// const authController = {
//   register: (req, res) => {
//     User.register(req.body)
//       .then(() => {
//         // res.redirect('/login')
//         res.render('index', {
//           title: 'berhasil daftar user baru!'
//         })
//       })
//       .catch(err => {
//         console.log(err)
//         res.render('error', err)
//       })
//   },
//   login: passport.authenticate('local', {
//     successRedirect: '/whoami',
//     // atau '/profile'
//     failureRedirect: '/users/login',
//     failureFlash: 'invalid username or password'
//   }),

//   loginJwt: (req, res) => {
//     User.authenticate(req.body)
//       .then(user => {
//         res.json(formatUser(user))
//       })
//       .catch((err) => {
//         res.send(err.message);
//       })
//   },

//   whoami: (req, res) => {
//     /* req.user adalah instance dari User Model, hasil autentikasi dari passport. */
//     const {
//       user
//     } = req;
//     res.json({
//       id: user.dataValues.id,
//       username: user.dataValues.username
//     })
//   },

//   restrictTo: (...roles) => {
//     return (req, res, next) => {
//       // roles = ['admin', 'player']
//       if (!roles.includes(req.user.role)) {
//         return next(new AppError('You do not have permission to perform this action!', 403)); //unauthorized
//       }
//     };
//   }
// };

// module.exports = authController;
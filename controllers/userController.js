const {
  User,
  Match
}
require = ('../models');

const userController = {
  getUser: async function (req, res) {
    const userId = req.params.id;
    try {
      const userData = await User.findByPk(1, {
        where: {
          id: userId
        },
        include: {
          model: Match
        }
      });

      return res.json(userData);
    } catch (err) {
      res.send("Error", err.message);
    }
  },
  getAllUsers: async function (req, res, next) {
    try {
      const users = await User.findAll({
        include: {
          model: Match,
        }
      });
      return res.json(users);
    } catch (error) {
      res.send("Error: ", error.message);
    }
  },
  getUserHistory: async function (req, res) {
    const userId = req.params.id;
    try {
      const userHistory = await User.findByPk(userId, {
        include: {
          model: Match
        }
      });
      return res.json(userHistory);
    } catch (err) {
      res.send("Error", err.message)
    }
  },
  updateUser: async function (req, res, next) {
    try {
      const updateUsername = await User.update({
        where: {
          username
        }
      })
      if (!username) {
        return next(new AppError("No room found with that ID!", 404))
      }
      res.status(201).json({
        status: 'Updating username success!',
        data: {
          username: updateUsername
        }
      })
    } catch (error) {
      res.send("Error: ", error.message)
    }
  },
  deleteUser: async function (req, res, next) {
    try {
      const subject = await User.destroy({
        where: {
          userId: req.params.id
        }
      });
      if (!subject) {
        return next(new AppError("No room found with that ID!", 404))
      }
      res.status(204).json({
        message: "Delete User success",
        data: null
      })
    } catch (error) {
      res.send("Error", error.message);
    }
  }
}

module.exports = userController;